---
layout: post
title:  "Cultivating Open-Source Agriculture in Tzoumerka: 15-17 September 2019"
date:   2019-09-06 00:00:00 -0300
location: Kalentzi, Northern Tzoumerka, Greece
categories: news
featured: '/images/2019_07_16-Open_Call_MNT_01.jpg'
excerpt: "The Municipality of Northern Tzoumerka invites you to a 3-day retreat titled 'Cultivating Open-Source Agriculture in Tzoumerka'."
---


The  **Municipality of Northern Tzoumerka**, in collaboration with the **P2P Lab**, are happy to announce that the results of the evaluation of the proposed small-scale agricultural solutions submitted for the three-day international agricultural co-design and distributed manufacturing retreat called **Tzoumakers: Cultivating Open-Source Agriculture in Tzoumerka**.


The **Municipality of Northern Tzoumerka** invites you to a 3-day retreat titled "Cultivating Open-Source Agriculture in Tzoumerka". In three days, farmers, makers, scholars and enthusiasts will explore new ways to deploy advanced and traditional technologies for local agriculture, through discussions and the co-creation of four open design tools and solutions.


The event is taking place on **15-17 September 2019** at the Tzoumakers rural makerspace in Kalentzi, Tzoumerka. Participation in all discussions, activities and making is open to everyone.


**Location**

[https://goo.gl/maps/7dAjCLtZBSu](https://goo.gl/maps/7dAjCLtZBSu)


**Program (pdf)**


[3DAY RETREAT PROGRAM - EN]({{ "/assets/files/2019_09_06_3DAY_RETREAT_EN_WEB.pdf" | prepend: site.baseurl }})

[3DAY RETREAT PROGRAM - EL]({{ "/assets/files/2019_09_06_3DAY_RETREAT_GR_WEB.pdf" | prepend: site.baseurl }})