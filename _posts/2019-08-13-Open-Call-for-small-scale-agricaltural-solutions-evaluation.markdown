---
layout: post
title:  "Open Call for small-scale agricultural solutions – Evaluation of proposals"
date:   2019-08-13 00:00:00 -0300
location: Kalentzi, Northern Tzoumerka, Greece
categories: news
featured: '/images/2019_07_16-Open_Call_MNT_01.jpg'
excerpt: "The Municipality of Northern Tzoumerka, in collaboration with the P2P Lab, are happy to announce that the results of the evaluation of the proposed small-scale agricultural solutions submitted for the three-day international agricultural co-design and distributed manufacturing retreat called “Tzoumakers: Cultivating Open-Source Agriculture in Tzoumerka”."
---


The  **Municipality of Northern Tzoumerka**, in collaboration with the **P2P Lab**, are happy to announce that the results of the evaluation of the proposed small-scale agricultural solutions submitted for the three-day international agricultural co-design and distributed manufacturing retreat called **“Tzoumakers: Cultivating Open-Source Agriculture in Tzoumerka”**.

Overall, eight (8) applications have been submitted before the final deadline, by local and international creative teams of makers, farmers and enthusiasts. All of them described interesting and useful small-scale, open-design solutions for the local agricultural sector. We would like to heartily thank everyone who has been involved in the development and submission of ideas, even though only four of the proposed solutions have to be selected to participate in the event.

The selection has been made based on a set of simplified criteria, concerning the development of the solution, the process and characteristics of its implementation and the expected impact for the local agricultural sector.

The results of the evaluation of all the submitted proposals, in descending order, is available [here](https://gitlab.com/phygitalproject/phygitalproject.gitlab.io/blob/master/assets/files/Selection.pdf).

It should be noted that the evaluation of the proposals at this stage is not to be considered binding to any of the parties involved. It rather states the organisers’ assessment of the proposed solutions, based on the information provided in the application form. The eventual participation and development of the selected solutions is subject to the availability of the applicants, the feasibility of the actual materialisation of the proposed solution in the context of the event or any other unforeseen factors. In case of non-fulfilment of these conditions by any of the selected teams, the organisers will proceed with the selection of the solution presenting the next highest score. 




