---
layout: post
title:  "Free/Libre Technologies, Arts and the Commons"
date:   2019-05-14 00:00:00 -0300
location: Nicosia, Cyprus
categories: news
featured: '/images/2019_05_14-unconference_header.jpg'
excerpt: "An Unconference about Art, Design, Technology, Making, Cities and their Communities"
---

**An Unconference about Art, Design, Technology, Making, Cities and their Communities** - University of Nicosia Research Foundation, as part of the project Phygital

[https://unrf-unconference.futureworldscenter.org/](https://unrf-unconference.futureworldscenter.org/)

# About the Event

*Unconference Format*

This Unconference follows the momentum of a broader movement rethinking the academic conference format towards a more connected model of knowledge sharing, peer learning and collaboration. This allows presentations of research while it also allows participants to work together and set their own agenda in discussions, workshops, and co-working sessions, that respond to previous proposals as well as spontaneously emerging priorities. We will be hosting participants from a broad network of researchers and activists across fields, and connecting remotely with others.

The Unconference participants are invited to contribute to an Open Access Online Masterclass with the same theme. The event will connect with local makeathons and later lead to an Exhibition that aims to communicate visually and interactively the outcomes of these debates. Developing a publication is one of the unconferenceâs main aims.

**Thursday Evening May 30th: Keynote**

**Friday, May 31st:** A day of presentations and pre-organised panels, during which we will collectively formulate a plan for the Saturday, according to emerging priorities among the participants

**Saturday, June 1st:** Discussions, workshops and co-working sessions designed according to the conclusions of the previous day.


**Provisional Themes**

- the technology of participatory art, community art, and socially engaged art

- the art of the free and open source software movement

- social media mobilisation: necessary or abusive?

- free and open source community technologies and arts

- openness and/vs freedom in social, art, design and tech initiatives

- hacktivisms and artivisms / hacking the art and art hacking

- contemporary questions around community and civically driven art, design and technology

- hacking as culture

- hackerspaces and makerculture / hackerspaces and their politics

- shared learning - peer to peer

- "Design Global Manufacture Local"

- local vs global dimensions in community practice

- tech and art resistance discourses, their directions and overlaps

- freedom tech, tech freedom

- making and the commons: redefining public art / participatory art

- digital commons

- digital governance and social movements

- self-organisation discourse

- open data discourse

- networked cultures

- technology and policy / policy tech

**Invited Speakers**

- Richard Stallman

- Silvia Federici

- Gregory Sholette

- Luiz Guilherme Vergara

- Johan Söderberg

- Gabriele de Seta

- Harriet Poppy Speed & Dr Lynn Jones

- Ruth Catlow

**Unconference Scientific Committee (Read more about team)**

Evanthia Tselika, University of Nicosia. (Chair)

Chrystalleni Loizidou, hack66/ Lakatamia Municipality. (Chair)

Helene Black, NeMe.org.

Yiannis Colakides, NeMe.org.

Maria Hadjimichael, University of Cyprus.

Marios Isaakidis, University College London.

Eva Korae, Cyprus University of Technology.

Thrasos Nerantzis, Future World Centre.

Leandros Savvides, Leicester University.

Gabriele de Seta, Institute of Ethnology, Academia Sinica in Taipei, Taiwan.

Niki Sioki, University of Nicosia.




**Location**

University of Nicosia

Makedonitissis 46, 

Nicosia 2417

Cyprus

**Contact**

For more information you can contact

Evanthia Tselika

Email: tselika.e@unic.ac.cy

Phone: +35722842694

[![Unconference Logo]({{ "/images/2019_05_14-unconference_LOGO.png" | prepend: site.baseurl }})]({{ "/images/2019_05_14-unconference_LOGO.png" | prepend: site.baseurl }})

[Unconference Programme - en]({{ "/assets/files/2019_05_14-Unconference_Programme.pdf" | prepend: site.baseurl }})
