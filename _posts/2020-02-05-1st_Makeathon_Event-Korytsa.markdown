---
layout: post
title:  "1st Makeathon Event, Korytsa"
date:   2020-02-05 00:00:00 -0300
location: Korçë-Albania
categories: news
featured: '/images/2020_02_05_1st_Makeathon_Invitation_header.png'
excerpt: "HEC Foundation invites you on Saturday 15/02/2020 at the 1 st Makeathon Event, an
interactive one, a creative one that brings together: ideas, skills, electronic machines,
entrepreneurs and young people."
---

# 1st Makeathon Event, Korytsa
**HEC Foundation** invites you on Saturday 15/02/2020 at the 1 st Makeathon Event, an
interactive one, a creative one that brings together: ideas, skills, electronic machines,
entrepreneurs and young people.

The #KorytsaMakerspaces will give you access to free space and tools. Make your ideas
reality with our project support!
In order to learn what a #Makerspace is, join us!


# Moderator, project staff and guest speakers:

Moderator, project staff and guest speakers:

\#MarielaStefanllari

\#NikolinDrabo

\#XhulianaPapamihal

\#JoanDeli

\#EleinaQirici

# When

Saturday, February 15, 2020, 11:00-16:00

# Location 

Korçë-Albania

# Contact

Nikolin Drabo - Project Coordinator

Email: drabonikolin@gmail.com

Phone: +355 683338665



Mariela Stefanllari - Project Manager

Email: hec.foundation@yahoo.com

Phone: +355 673133777


# INVITATION
[INVITATION]({{ "/assets/files/2020_02_05_1st_Makeathon_Invitation_15February_2020.pdf" | prepend: site.baseurl }})

# AGENDA
[AGENDA]({{ "/assets/files/2020_02_05_1st_Makeathon_Agenda_15_February_2020.pdf" | prepend: site.baseurl }})


