---
layout: post
title:  "Infoday@Ioannina Event Agendas"
date:   2019-01-02 12:00:00 -0300
location: Ioannina, Greece
category: news
---

(text in english follows)

*ΠΡΟΣΚΛΗΣΗ*

Ο Δήμος Βορείων Τζουμέρκων σας προσκαλεί στην Ημερίδα Ενημέρωσης για το έργο Phygital.

Η εκδήλωση θα πραγματοποιηθεί την Παρασκευή, 11 Ιανουαρίου και ώρα 17:30 στην αίθουσα Κ. Φρόντζος στο συγκρότημα [«Φρόντζου Πολιτεία»](group.frontzupolitia.gr/contact/) στα Ιωάννινα.

*Αμέσως πριν την έναρξη της εκδήλωσης, στις 16:30, θα πραγματοποιηθεί συνέντευξη τύπου με τους βασικούς συντελεστές του έργου. 

Το έργο Phygital στοχεύει στην ανάπτυξη και την πιλοτική εφαρμογή του εναλλακτικού παραγωγικού μοντέλου «σχεδιάζουμε παγκόσμια, κατασκευάζουμε τοπικά», το οποίο προκύπτει από τη σύζευξη παγκόσμια ανοικτής γνώσης, λογισμικού και ανοικτού σχεδιασμού, με τεχνολογίες αποκεντρωμένης κατασκευής τοπικά. Το έργο υλοποιείται στο πλαίσιο του Προγράμματος Διακρατικής Συνεργασίας Interreg V-B «Βαλκανική – Μεσόγειος 2014-2020» με τη συνεργασία φορέων από Ελλάδα, Κύπρο και Αλβανία. 

*INVIΤATION*

The Municipality of Northern Tzoumerka kindly invites you to the Infoday for project Phygital.

The event will take place on Friday, January 11 at 17:30 in K. Frontzos venue of the [“Frontzos Politeia”](group.frontzupolitia.gr/en/contact/) complex in Ioannina.

*Prior to the event, at 16:30, a press conference will be held with the project’s main instigators. 

Phygital aims for the development and pilot implementation of the alternative productive model codified as “design global, manufacture local”, which builds on the confluence of global open knowledge, software and design, with local distributed manufacturing technologies. The project is implemented under the Transnational Cooperation Programme Interreg V-B “Balkan – Mediterranean 2014-2020” by cooperating entities from Greece, Cyprus and Albania.

*Infoday@Ioannina agendas*

[agenda - en]({{ "/assets/files/infoday-ioannina-Invitation_EN.pdf" | prepend: site.baseurl }})

[agenda - el]({{ "/assets/files/infoday-ioannina-Invitation_GR.pdf" | prepend: site.baseurl }})