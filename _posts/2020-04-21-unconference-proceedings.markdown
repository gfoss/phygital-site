---
layout: post
title:  "Proceedings of the Unconference 30th May - 1st June 2019"
date:   2020-04-21 00:00:00 -0300
location: Nicosia, Cyprus
categories: news
featured: '/images/2020-04-21-Unconference_Proceedings_Phygital-header_02.jpg'
excerpt: "More than a hundred people took part in the Unconference Free/Libre Technologies, Arts and the Commons, part of the project Phygital, that took place from Thursday, May 30th until Saturday, June 1st 2019 at the University of Nicosia in Cyprus"
---


An **Unconference** about Art, Design, Technology, Making, Cities and their Communities - University of Nicosia Research Foundation, as part of the project Phygital. More than a hundred people took part in the Unconference Free/Libre Technologies, Arts and the Commons, part of the project Phygital, that took place from Thursday, May 30th until Saturday, June 1st 2019 at the University of Nicosia in Cyprus.

[https://phygitalproject.eu/news/2019/05/14/Unconference-about-Art-Design-Technology-Making-Cities-and-their-Communities.html](https://phygitalproject.eu/news/2019/05/14/Unconference-about-Art-Design-Technology-Making-Cities-and-their-Communities.html)


[UNCONFERENCE PROCEEDINGS]({{ "/assets/files/2020-04-21-Unconference_Proceedings_Phygital.pdf" | prepend: site.baseurl }})
