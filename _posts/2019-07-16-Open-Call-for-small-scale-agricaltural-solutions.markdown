---
layout: post
title:  "Open Call for small-scale agricaltural solutions"
date:   2019-07-16 00:00:00 -0300
location: Kalentzi, Northern Tzoumerka, Greece
categories: news
featured: '/images/2019_07_16-Open_Call_MNT.png'
excerpt: "Open Call for small-scale agricaltural solutions"
---

The **Municipality of Northern Tzoumerka**, in collaboration with the P2P Lab, are happy to announce the launch of the three-day international agricultural co-design and distributed manufacturing retreat called **"Tzoumakers: Cultivating Open-Source Agriculture in Tzoumerka"**. 


# About the Event

The event will be centered around the **rural makerspace "Tzoumakers"**, [located in Kalentzi](https://goo.gl/maps/qi1pdoNVD9MBkVt88), in the Municipality of Northern Tzoumerka, Greece. 


We are looking for four (4) creative teams of designers, makers, farmers or enthusiasts to join the event in Kalentzi and co-create open-source, small-scale solutions for the agricultural sector. Selected teams, of up to four (4) persons per team, will introduce their solutions to the local community and manufacture it with their help in three days, keeping local biophysical conditions in mind.

# Apply

To apply for this call, please fill in the application form in [this link](https://forms.gle/E2p2Qs2Ko1ACS4Ls7).

The deadline for the submission of applications is Wednesday, 31 July 2019 22:00 CET.

The workshop will take place in mid-September (exact dates to be confirmed).

For more details, selection criteria and timeline, see the attached file. 

For queries, you may contact us at research@p2pfoundation.net 


[Open Call MNT - pdf file]({{ "/assets/files/2019_07_16-Open-Call-MNT.pdf" | prepend: site.baseurl }})
