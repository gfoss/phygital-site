---
layout: post
title:  "Workshop by University of Nicosia Research Foundation"
date:   2019-02-15 00:00:00 -0300
location: Egkomi, Cyprus
categories: news
featured: '/images/2019_02_20_workshop_header_Jenny_Dunn.jpg'
excerpt: "Design Led Approach to Social Practice: Making Spatial Interventions for Test Sites of Exchange. A University of Nicosia Researh Foundation workshop for the project Phygital by Jenny Dunn"
---

**A**  **Workshop by**  **Jenny Dunn** University of Nicosia Research Foundation


Design Led Approach to Social Practice: Making Spatial Interventions for Test Sites of Exchange

# University of Nicosia Research Foundation

This workshop will address social art and design practice strategies that build community agency, capacity and resilience through cultural production:

- Commoning, Sharing food, culture, and history
- Making Visible, sharing and giving a platform for existing voices and existing cultures
- Occupying Space, trying things in 1:1 scale.

Together in groups we will use a design led approach to build temporary spatial interventions as test sites of exchange; spaces to meet, discuss and share. How could these spaces allow for mapping and sharing local narratives, skills and existing cultures in order to inform projects and shared cultural production? We will be discussing and working with design processes such as strategic planning, mapping, drawing, collaging, model making and prototyping.



**Workshop** Wednesday, 20 February 2019

18.00-21.00

**Location**

Fine Art Building

Michail Georgalla 29

Egkomi

Cyprus

[https://goo.gl/maps/7dAjCLtZBSu](https://goo.gl/maps/7dAjCLtZBSu)

**Facebook Event** 

[https://www.facebook.com/events/2195547847220820/](https://www.facebook.com/events/2195547847220820/)

**Contact**

For more information you can contact

Evanthia Tselika

Email: tselika.e@unic.ac.cy

Phone: +35722842694

[![Workshop brochure]({{ "/images/2019_02_20_workshop_flyer_Jenny_Dunn.jpg" | prepend: site.baseurl }})]({{ "/images/2019_02_20_workshop_flyer_Jenny_Dunn.jpg" | prepend: site.baseurl }})
