---
layout: post
title:  "Open Call Unconference: Free and Open Source Technologies, Art and Commoning Practices"
date:   2018-12-07 12:00:00 -0300
location: Nicosia, Cyprus
category: news
---

*Free and Open Source Technologies, Arts and Commoning Practices*
*An Unconference about Art, Design, Technology, Making, Cities and their Communities*

*31/05-02/06*

*University of Nicosia Research Foundation, Nicosia, Cyprus*

PHYGITAL project. Co-organized in collaboration with Lakatamia Municipality/hack66 and the Fine Arts Programme, Department of Design and Multimedia, University of Nicosia.

*What kind of creativity comes after today's digital cultures? After the smart city, post-surveillance, post-innovation, when social entrepreneurship discourse has ran its course?*

 In the last few years there has been a sharp momentum in the growth of  groups and spaces that operate under collective and community- driven structures of collaboration and shared learning processes (onsite and online). This is happening in parallel to   greater debates around the fate of the commons, openness, freedom of access and how new digital scapes are influencing how we shape socially and community orientated art, design and technological practices.

The unconference brings together scholars and practitioners across fields, to convene, share, and collaborate on issues around the physical and digital commons, the free and open source art and technology movements, as well as collective and community- driven structures of collaboration and shared pedagogic processes (onsite and online).

*We invite 15-20min presentations, panels, abstracts, posters, and workshop/unpanel proposals by 28/02/2019.*

*Updates can be found*

[here](http://www.unrf.ac.cy/free-and-open-source-technologies-arts-and-commoning-practices-an-unconference-about-art-design-technology-making-cities-and-their-communities/)

*Or*

[here](https://fineartuniccy.wordpress.com/conferences-and-academic-meetings/free-and-open-source-technologies-arts-and-commoning-practices-an-unconference-about-art-design-technology-making-cities-and-their-communities/)


*The Unconference will be free of charge.*

*To propose something please send:*

Panels: 250 word abstracts of all presenters, short bios, contact details and panel title

Paper: 250 word abstract, short bio, contact details and paper title

Workshop: 250 word abstract, short bio, contact details and paper title

Other ideas: 250 word abstract/ concept note, short bio and contact details

Submit the above information to: [cycommons@protonmail.com](mailto:cycommons@protonmail.com)

For any queries do email [tselika.e@unic.ac.cy](mailto:tselika.e@unic.ac.cy) or [chrystalleni.loizidou@gmail.com](mailto:chrystalleni.loizidou@gmail.com)


[Open Call PDF]({{ "/assets/files/UNCONFERENCE_tech_art_commons_UNRF_2019.pdf" | prepend: site.baseurl }})