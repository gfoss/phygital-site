---
layout: post
title:  "Agenda for the presentation of cultural heritage projects, agricultural production, and social practices in the urban space."
date:   2020-12-04 00:00:00 -0300
location: Online, Greece
categories: news
featured: '/images/2020_12_04-eellak-invitation-agenda-header.png'
excerpt: "Open Technologies Organization - GFOSS invites you to the three-day online event of Phygital project on 14, 15, and 16 December 2020."
---

Open Technologies Organization - GFOSS within the Phygital project organizes a three-day online event on December 14, 15, and 16, 2020, where makers, collectives and initiatives from Greece and abroad will present projects in the fields of cultural heritage, agricultural production , and social practices in urban areas.


The event will present the main actions and results of the project, focusing on three makerspaces in Greece, Cyprus and Albania and the original solutions built by local communities, based on global digital knowledge, software and design communities.


Each day closes with a section open for co-creation and discussion with participants.

To attend the event register [here](https://ellak.gr/simmetochi/){:target="_blank"}


 &nbsp;
 
 
[Invitation Agenda (EL)]({{ "/assets/files/2020_12_04-TRIHMERH-DIADIKTUAKH-EKDHLWSH.pdf" | prepend: site.baseurl }})


[Invitation Banner (EL)]({{ "/images/2020_12_04-eellak-invitation-agenda-banner.png" | prepend: site.baseurl }})


 &nbsp;
 
 