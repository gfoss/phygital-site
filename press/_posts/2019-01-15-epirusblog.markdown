---
layout: post
title:  "Article on 'epirusblog.gr' blog for the Information Workshop for the Phygital project"
date:   2019-01-15 12:00:00 -0300
location: Ioannina, Greece
category: press
---

The ['epirusblog'](http://www.epirusblog.gr/) article devoted to the organization of the Workshop which was carried out by the Municipality of Northern Tzoumerka as part of the communication actions that constitute the Municipality's contribution to the implementation of the project. 

[Article link](http://www.epirusblog.gr/2019/01/phygital.html)