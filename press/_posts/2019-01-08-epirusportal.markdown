---
layout: post
title:  "Article on 'epirusportal.gr' local news portal for the Phygital project"
date:   2019-01-08 12:00:00 -0300
location: Ioannina, Greece
category: press
---


The ['epirus portal'](http://www.epirusportal.gr/) article mentioned the organization of the Workshop who was carried out by the Municipality of Northern Tzoumerka as part of the communication actions that constitute the Municipality's contribution to the implementation of the project and information about Phygital project.


[Article link](https://www.epirusportal.gr/%ce%b7%ce%bc%ce%b5%cf%81%ce%af%ce%b4%ce%b1-%ce%b5%ce%bd%ce%b7%ce%bc%ce%ad%cf%81%cf%89%cf%83%ce%b7%cf%82-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-%ce%ad%cf%81%ce%b3%ce%bf-phygital/)