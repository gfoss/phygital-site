(function($){
	var $body = $('body')
	var $window = $(window)

	var app = {
		init: function(){
			var that = this
			$window.scroll(function(){
				that.scrollHandlers()
			})
			this.copyCode()
			this.enableRellax()
			this.openMenu()
		},
		
		scrollHandlers: function(){
			if($window.scrollTop() > 50){
				$('#site-header').addClass('collapsed')
			}
			else{
				$('#site-header').removeClass('collapsed')
			}
		},

		openMenu: function(){
			$('#burger').click(function(){
				$(this).toggleClass('burger-open')
				$body.toggleClass('menu-open')
			})
		},

		enableRellax: function(){
			if($('.rellax').length){
				var rellax = new Rellax('.rellax')
			}
		},

		copyCode: function(){
			var copyText = $('.copy-banner-code').click(function(evt){
				
				cpText = $(evt.target)
				.parent('.banner-code')
        .clone()    //clone the element
        .children() //select all the children
        .remove()   //remove all the children
        .end()  //again go back to selected element
				.text()
				
				var tempEl = document.createElement('input')
				tempEl.type = 'text'
				// tempEl.className = 'no-dis'
				tempEl.value = cpText
				console.log(tempEl)

				var $tempEl = $(tempEl)
				$(evt.target).parent().append($tempEl)
				
				tempEl.select()
				document.execCommand("copy");
				$tempEl.remove()
			})
		}
	}

	$(function(){
		app.init()
	})
})(jQuery)