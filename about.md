---
layout: info_page
title: About
type: main
slug: about
permalink: /about/
menus:
  main-nav:
    weight: 3
---

The starting point of the project has been a socio-institutional problem and a tentative techno-economic solution. In the context of the recent developments in the social economic sector, an increasing number of people has been contributing to social innovation projects, striving to create sustainable options for surpassing the deep economic, social and environmental crisis in Europe. However, these communities face certain challenges in creating sustainable livelihoods and in supporting the appropriate organisational and governance structures. At the same time, the wide diffusion of ICT and the decentralisation of the means of information production pose as an untapped opportunity, in providing new techno-economic blueprints for sustainability.

PHYGITAL aims to pilot, evaluate and promote an emerging production and business model, based on the conjunction of a global digital commons of knowledge, stemming from various open source projects, with distributed manufacturing technologies, such as three-dimensional (3D) printers and Computerised Numerical Control (CNC) machines. Seeds of this model, codified as “design global, manufacture local” (DGML), have been exemplified by numerous successful commons-oriented projects on a global level, introducing innovative organisational and business patterns. The project is expected to increase local capacities for innovation, by harnessing global knowledge to address local challenges. At the same time, strong transnational cooperation linkages will be created, in support of innovative entrepreneurial ventures, including social enterprises.

PHYGITAL adopts a multi-level approach, in order to bring together diverse stakeholders and promote synergy on local, transnational and global level, by: (a) developing a multilingual open knowledge platform, which will host, organise & diffuse the global knowledge commons; (b) developing and connecting open collaborative production spaces (makerspaces), equipped with distributed manufacturing technologies, to catalyse the relevant physical practices in scalable pilot programmes, taking place in 3 thematic areas: agriculture (Greece); social arts practices (Cyprus) & cultural heritage (Albania) and (c) supporting the emerging entrepreneurial practices through the Phygital Network, an open, decentralised network of businesses & professionals, dedicated to the empowerment of innovation and social entrepreneurship on transnational level.

The PHYGITAL target groups include restless entrepreneurs and freelancers; local SMEs and micro-enterprises from different sectors, as well as activists, hobbyists and communities contributing to open source and social innovation projects and the wider public. The added value of the project lies in the empowerment of more people to adopt sustainable patterns and work towards making a positive social impact, by co-designing and co-producing new solutions for local challenges and openly sharing the benefits on a global level. 