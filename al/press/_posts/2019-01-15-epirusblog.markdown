---
layout: post
langUrl: /al
title:  "Artikull në blog-un 'epirusblog.gr' për Workshop-in Informativ të Projektit Phygital"
date:   2019-01-15 12:00:00 -0300
location: Ioannina, Greece
category: al-press
---

Artikulli i publikuar në blog-un ['epirusblog'](http://www.epirusblog.gr/) i kushtohet organizimit të një Workshop-i i cili u zhvillua nga Bashkia e Xumerkës së Veriut si pjesë e strategjisë së komunikimit dhe të drejtës së informimit të publikut në lidhje me kontributin e Bashkisë në implementimin e projektit Phygital. 

[Article link](http://www.epirusblog.gr/2019/01/phygital.html)