---
layout: post
langUrl: /al
title:  "Konferencë shtypi për teknologjitë e hapura në Xumerkë"
date:   2018-04-02 12:00:00 -0300
location: Thessaloniki, Greece
category: al-press
---

Transmetimi "Antidrastirio" është i dedikuar në zhvillimin dhe përhapjen e teknologjive të hapura në komunitetin e Xumerkës. 

[ ERT3 link (video)(in greek)](http://www.ert.gr/ert3/ert3-protaseis/ert3-antidrastirio-tzoumerka-i-genisi-mias-koinotitas-anoihton-tehnologion-trailer/)