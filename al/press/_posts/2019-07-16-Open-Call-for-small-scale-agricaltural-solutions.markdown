---
layout: post
langUrl: /al
title:  "Thirrje e hapur për zgjidhje të shkallës së ulët në fushën e agrikulturës"
date:   2019-07-16 00:00:00 -0300
location: Kalentzi, Northern Tzoumerka, Greece
category: al-press
featured: '/images/2019_07_16-Open_Call_MNT_01.jpg'
excerpt: "Bashkia e Xumerkës së Veriut në bashkëpunim me P2P Lab, janë të lumtur të shpallin thirrjen për mbledhje interaktive të fokusuar në dizajnin ndërkombëtar të agrikulturës dhe ndërtimin kolektiv të titulluar 'Tzoumakers: Kultivimi i Burimeve të Hapura Agrikulturore në Xumerkës'."
---

**Bashkia e Xumerkës së Veriut**, në bashkëpunim me P2P Lab, janë të lumtur të shpallin thirrjen për mbledhje interaktive të fokusuar në dizajnin ndërkombëtar të agrikulturës dhe ndërtimin kolektiv të titulluar 'Tzoumakers: Kultivimi i Burimeve të Hapura Agrikulturore në Xumerkë.'**. 


# Në lidhje me eventin

Eventi do të fokusohet rreth **makerspace-it rural "Tzoumakers"**, [të ndodhur në Kalenxi](https://goo.gl/maps/qi1pdoNVD9MBkVt88), në bashkinë e Xumerkës së Veriut, Greqi. 


Jemi në kërkim të katër (4) ekipeve krijuese dizenjuesish, krijuesish, fermerësh ose entuziastësh për t'iu bashkuar eventit në Kalenxi dhe për të bashkëkrijuar zgjidhje me burime të hapura dhe në shkallë të ulët në sektorin agrikulturor. Ekipet e zgjedhura, të cilat mund të përbëhen nga deri në katër (4) persona, do të prezantojnë zgjidhjen e propozuar në komunitetin lokal dhe më pas do të ndërtojnë modelin e propozuar brenda tre ditëve, duke pasur parasysh kushtet bio-fizike lokale.

# Si të aplikoni

Për të aplikuar në këtë thirrje, ju lutem plotësoni formularin tek [this link](https://forms.gle/E2p2Qs2Ko1ACS4Ls7).

Afati përfundimitar i aplikimive është dita e mërkurë 31 Korrik 2019, ora 22:00.

Workshop-i do të zhvillohet në mes të muajit Shtator (datat ekzakte mbeten për t'u konfirmuar).

Për më shumë detaje, në lidhje me kriteret e pranimit dhe kohëzgjatjen e eventit konsultohuni me dokumentin bashkangjitur. 

Për pyetje të tjera, mund të na kontaktoni në adresën elektronike research@p2pfoundation.net -AL-


[Open Call MNT - pdf file]({{ "/assets/files/2019_07_16-Open-Call-MNT.pdf" | prepend: site.baseurl }})



