---
layout: post
langUrl: /al
title:  "Artikull në voreiatzoumerka.gr për Workshop-in Informativ të Projektit Phygital"
date:   2019-01-15 12:00:00 -0300
location: Ioannina, Greece
category: al-press
---

Artikulli i publikuar në ['voreiatzoumerka'](http://www.voreiatzoumerka.gr/) i kushtohet organizimit të një Workshop-i i cili u zhvillua nga Bashkia e Xumerkës së Veriut si pjesë e strategjisë së komunikimit dhe të drejtës së informimit të publikut në lidhje me kontributin e Bashkisë në implementimin e projektit Phygital. 

[Article link](http://www.voreiatzoumerka.gr/index.php/2015-09-08-12-10-48/2015-10-05-22-37-11/405-tzoumakers)