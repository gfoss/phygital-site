---
layout: post
langUrl: /al
title:  "Artikull në portalin typos-i.gr për Workshop-in Informativ të Projektit Phygital"
date:   2019-01-15 12:00:00 -0300
location: Ioannina, Greece
category: al-press
---

Artikulli i publikuar në portalin ['typos-i.gr](https://typos-i.gr/) i kushtohet organizimit të një Workshop-i i cili u zhvillua nga Bashkia e Xumerkës së Veriut si pjesë e strategjisë së komunikimit dhe të drejtës së informimit të publikut në lidhje me kontributin e Bashkisë në implementimin e projektit Phygital. 

[Article link](https://typos-i.gr/article/phygital-systh8hke-ston-kosmo)