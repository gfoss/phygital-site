---
layout: post
langUrl: /al
title:  "Artikulli në gazetën 'Neoi Agones' ('Beteja të Reja') për teknologjitë e hapura për komunitetin në Xumerkë"
date:   2018-04-26 12:00:00 -0300
location: Thessaloniki, Greece
category: al-press
---

Artikulli në gazetën ['Neoi Agones' ('Beteja të Reja')](http://neoiagones.gr/) i është kushtuar zhvillimeve dhe përhapjes së teknologjive të hapura për komunitetin në Xumerkë. 

[![Newspaper excerpt]({{ "/assets/files/press_coverage/2018.04.26.NeoiAgones.png" | prepend: site.baseurl }})]({{ "/assets/files/press_coverage/2018.04.26.NeoiAgones.png" | prepend: site.baseurl }})