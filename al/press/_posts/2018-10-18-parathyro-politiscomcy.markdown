---
layout: post
langUrl: /al
title:  "Artikull në revistën elektronike 'parathyro.politis.com.cy' kushtuar organizimit të një workshop-i nga Fondacioni Kërkimor i Universitetit të Nikosisë në kuadër të projektit PHYGITAL"
date:   2018-10-18 12:00:00 -0300
location: Lakatamia, Cyprus
category: al-press
---

Artikulli i publikuar në revistën elektronike ['POLITIS PARATHIRO'](http://parathyro.politis.com.cy/) i është kushtuar organizimit të një workshop-i të nga Fondacioni Kërkimor i Universitetit të Nikosisë në kuadër të projektit PHYGITAL that happened on October 11 and 13, 2018.

[Article link](http://parathyro.politis.com.cy/2018/10/ergastiri-gia-to-ergo-phygital-apo-to-erevnitiko-idryma-panepistimiou-lefkosias/)