---
layout: post
langUrl: /al
title:  "Artikull në blog-un topikopoiisi.eu për teknologjitë e hapura për komunitetin e Xumerkës"
date:   2018-05-14 12:00:00 -0300
location: Thessaloniki, Greece
category: al-press
---

Artikulli i publikuar në blog-un [topoikopoiisi.eu](http://www.topikopoiisi.eu/) i është kushtuar përhapjes dhe zhvillimit të teknologjive të hapura për komunitetin në Xumerkë. 

[Article link](http://www.topikopoiisi.eu/902rhothetarhoalpha/-phygital)