---
layout: post
langUrl: /al
title:  "Artikull në portalin lokal 'epirusportal.gr' për projektin Phygital"
date:   2019-01-08 12:00:00 -0300
location: Ioannina, Greece
category: al-press
---

Artikulli i publikuar në portalin lokal ['epirus portal'](http://www.epirusportal.gr/) i kushtohet organizimit të një Workshop-i i cili u zhvillua nga Bashkia e Xumerkës së Veriut, në kuadër të strategjisë së komunikimit dhe informimit publik në lidhje me kontributin që Bashkia ka dhënë në implementimin e projektit Phygital.

[Article link](https://www.epirusportal.gr/%ce%b7%ce%bc%ce%b5%cf%81%ce%af%ce%b4%ce%b1-%ce%b5%ce%bd%ce%b7%ce%bc%ce%ad%cf%81%cf%89%cf%83%ce%b7%cf%82-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-%ce%ad%cf%81%ce%b3%ce%bf-phygital/)