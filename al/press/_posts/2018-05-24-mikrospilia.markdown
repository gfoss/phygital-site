---
layout: post
langUrl: /al
title:  "Artikull në blog-un 'Ta nea tis Mikrospilias' për teknologjitë e hapura për komunitetin në Xumerkë"
date:   2018-05-24 12:00:00 -0300
location: Thessaloniki, Greece
category: al-press
---

Artikulli i publikuar në blog-un ['Ta nea tis Mikrospilias'](http://taneatismikrospilias24.weebly.com) i është kushtuar zhvillimit dhe përhapjes së teknologjive të hapura për komunitetin në Xumerkë. 

[Article link](http://taneatismikrospilias24.weebly.com/alpharhochiiotakappaeta-sigmaepsilonlambdaiotadeltaalpha/tzoumakers)