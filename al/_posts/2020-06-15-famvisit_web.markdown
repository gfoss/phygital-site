---
layout: post
langUrl: /al
title: "Vizitë e famshme për blogerët dhe gazetarët e udhëtimeve në Tzoumerka Veriore"
date: 2020-06-15 00:00:00 -0300
location: Tzoumerka Veriore, Greqi
categories: newsal
featured: '/images/2020_06_15-famvisit_Header.jpg'
excerpt: "Komuna e Tzoumerka Veriore është e kënaqur t'ju ftojë në vizitën e famshme për blogerët dhe gazetarët e udhëtimit në Tzoumerka Veriore, e cila do të zhvillohet nga 17 korrik deri më 19 korrik 2020."
---

Komuna e Tzoumerkës Veriore është e kënaqur t'ju ftojë në vizitën e famshme për blogerët dhe gazetarët e udhëtimeve në Tzoumerka Veriore, e cila do të zhvillohet nga 17 korrik deri më 19 korrik 2020. Ky udhëtim familjarizimi synon të nxjerrë në pah trashëgiminë natyrore dhe kulturore të gjerë. zona e Tzoumerka Veriore përmes publikimeve dhe postimeve të synuara në Mediat Sociale dhe në Mediat më të gjera. Vëmendje e veçantë do t'i kushtohet veprimeve të projektit Phygital dhe në veçanti hapësirave rurale "Tzoumakers" e vendosur në Kalentzi, në Komunën e Tzoumerka Veriore, Greqi.


Për të aplikuar për këtë thirrje, ju lutemi plotësoni formularin e mëposhtëm të aplikimit.

 
Afati i fundit për dorëzimin e aplikacioneve është e Premtja, 10 Korrik 2020.





[Formulari i aplikimit (EL)] ({{"/assets/files/2020_06_15-Aithsh.pdf" | prepend: site.baseurl}})

[Broshura e Ftesës (EL)] ({{"/assets/files/2020_06_15-D5.5.1.FAMVISIT_WEB.PDF" | prepend: site.baseurl}})