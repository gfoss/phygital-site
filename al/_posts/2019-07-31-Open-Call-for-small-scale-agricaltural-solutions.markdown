---
layout: post
langUrl: /al
title:  "Zgjatje e afatit të aplikimeve - Thirrje e hapur për zgjidhje agrikulturore të një shkalle të ulët"
date:   2019-07-31 00:00:00 -0300
location: Kalentzi, Northern Tzoumerka, Greece
categories: newsal
featured: '/images/2019_07_16-Open_Call_MNT_01.jpg'
excerpt: "Bashkia e Xumerkës së Veriut, në bashkëpunim me laboratorin P2P, janë të lumtur të njoftojnë zhvillimin e një mbledhjeje ndërkombëtare tre-ditor për dizanjin agrokulturor dhe ndërtimin të titulluar 'Tzoumakers: Kultivimi i Agrikulturës me Burime të Hapura në Xumerkë."
---

**Bashkia e Xumerkës së Veriut**, në bashkëpunim me laboratorin P2P, janë të lumtur të njoftojnë zhvillimin e një mbledhjeje ndërkombëtare tre-ditor për dizanjin agrokulturor dhe ndërtimin të titulluar **"Tzoumakers: Kultivimi i Agrikulturës me Burime të Hapura në Xumerkë"**. 


# About the Event

Eventi do të ketë si epiqendër **makerspace-in rural "Tzoumakers"**, [të ndodhur në Kalenxi](https://goo.gl/maps/qi1pdoNVD9MBkVt88), në Bashkinë e Xumerkës së Veriut, Greqi. 


Jemi në kërkim të katër (4) ekipeve dizenjuesish, krijuesish, fermerësh ose entuziastësh për t'iu bashkuar eventit në Kalenxi dhe për të bashkëkrijuar zgjidhje burimesh të hapura dhe të një shkalle të vogël për sektorin agrokulturor. Ekipet e përzgjedhura, që duhet të kenë jo më shume se katër (4) persona për ekip, do të prezantojnë zgjidhjet e tyre tek komuniteti lokal dhe do i ndërtojnë ato në tre ditë, duke mbajtur ndërmend kushtet lokale bio-fizike.

# Si të aplikoni

Për të aplikuar ju lutem plotësoni formularin në [këtë link](https://forms.gle/E2p2Qs2Ko1ACS4Ls7).

**Afati i ri** për dorëzimin e aplikacioneve është dita e Premte, 9 Gusht 2019, ora 22:00.

Ky workshop do të zhvillohet në mes të muajit Shtator (datat ekzakte mbeten për t'u konfirmuar).

Për më shumë detaje, kriteret e përzgjedhjes dhe kohëzgjatjen, shikoni dokumentin e bashkangjitur. 

Nëse keni pyetje mund të na kontaktoni në research@p2pfoundation.net 



[Open Call MNT - pdf file]({{ "/assets/files/2019_07_31-Open-Call-MNT_ext.pdf" | prepend: site.baseurl }})
