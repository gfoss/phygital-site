---
layout: post
langUrl: /al
title:  "Eventi Paralel i Ekspozitës së parë të Phygital"
date:   2019-09-20 00:00:00 -0300
location: Nicosia, Cyprus
categories: newsal
featured: '/images/2019_09_20-exhibition_1st_parallel.jpg'
excerpt: "Ekspozita e Phygital & Eventet. E bashkëorganizuar nga Fondacioni Kërkimor i Universitetit të Nikosisë dhe Bashkia e Lakatamisë. Partneri implementues: Thkio Ppalies. I bashkëkuruar nga Peter Eramian, Elena Parpa, Evi Tselika. Dizajni nga Nico Stephou."
---

# Ekspozia Phygital 
**Fondacioni Kërkimor** i Universitetit të Nikosisë dhe **Bashkia e Lakatamisë** | Partner implementues, **Thkio Ppalies**

# Programi paralel i eventeve të ekspozitës të Phygital | Makeathon

Ekspozita **"drapër & kod"** hapet me leksionin "Si të besh kauçuk". Një leksion i drejtuar nga artisti **Nihaal Faizal**

# Kur

28 Shtator 2019, 17:00-20:00

# Vendndodhja 

Vendi | Thkio Ppalies, Nikosi
(2b Kissamou, Palouriotissa, Nicosia 1040)

# Kapaciteti 

20 pjesëmarrës mbi moshën 15 vjeç

# Regjistrimi

info@thkioppalies.org

+35722842694


"Shtëpi kauçuku, makina kauçuku,
Mastikë kauçuku dhe ëmbëlsira,
Këpucë kauçuku për këmbët e lodhura,
Muzikë me një ritëm kauçuku.
 
Insekte kauçuku, aviona kauçuku.
Filtra cigaresh prej kauçuku.
Asnjë shqetësim, asnjë brengë,
Kauçuk, Kauçuk, ngado."
 
"Kënga e Kauçukut" nga Richard M. Sherman dhe Robert B. Sherman
 
 
Duke përdorur videot e disponueshme online që demonstrojnë sesi mund të ndërtosh kauçukun, leksioni-workshop do të eksplorojë historinë e kauçukut, një substancë gjysëm-fiksionale e popullarizuar nga një seri filmash Diznei prej vitit 1961 deri në 1997, duke e shndërruar atë në një objekt ideologjik.

Fillimisht duke u shfaqur në një tregim të shkurtër në vitin 1942, kauçuku është një substancë e cila ka një marrëdhënie intime me histori të ndryshme në kontekstet e ndryshme ku është shfaqur. Ai ka marrë formën e shumë objekteve të imituara, si goma artificiale, substancë për përdorim ushtarak, një lojë fëmijësh, një substancë për kontrollin e kohës dhe deri tek një qenie e ndjeshme dhe inteligjente. Transformimet konceptuale të Kauçukut, i bëjnë eko aspiracioneve dhe dëshirave fiksionale dhe komerciale të producentëve të tij. 

Në përgjigje formës së hollë të kauçukut dhe "paketemit" që i bëri Disney si një material miqësor dhe i padëmshëm, kauçuku është sot një nga objektet më populore të përdorura për lojë. Siç vihet në dukje dhe nga materiali filmik që do të shfaqet në leksion, kauçuku nuk është një objekt loje vetë në përdorimin e tij final, por është edhe i tillë përgjatë kohës së përpunimit të tij. Duke përdorur pajisje shtëpiake të së përditshmes, pjesëmarrësit do të mund të prodhojnë figura kauçuku, teksa do të eksplorojnë rolet historike dhe funksionet që ky material ka pasur.  

 
# Shënim Biografik

**Nihaal Faizal** është një artisti, puna e të cilit dokumenton krijime me përdorim të burimeve teknologjike të ndryshme. 
Në të shkuarën, ai ka krijuar video, peisazhe për në Desktop, antikuare artificiale, fotografi familjare, filma popullorë, video online dhe shumë krijime të tjera. Në përgjigje të këtyre dokumentave, puna e tij adreston pyetje rreth të drejtës së autorësisë, teknologjisë, memorjes kulturore dhe materializmit. 
Së fundmi ai ka qenë pjesëmarrës në Programin "HomeWorkspace" në Ashkal Alwan (që ndodhet në Bejrut të Lebanonit), si dhe ka themeluar [Reliable Copy](http://www.reliablecopy.org/) një shtëpi botuese për punët, projektet dhe shkrimin nga artistët (në Bangalore të Indisë). Disa prej ekspozitave të tij në vazhdim e sipër dhe disa të tjera që pritet të materializohen së shpejti janë 'View:India' në muzeun Landskrona (në Suedi) dhe 'Sickle-n-Code' në qendrën e Trashegimnisë Kulturore në Bashkinë e Lakatamisë (në Qipro). 


**drapër dhe kod hapet më 24 Tetor 2019, në orën 18:00 në Muzeun e Historisë dhe Trashëgimnisë Kulturore në Lakatami, Agias Paraskevis.
[https://goo.gl/maps/suVx2PvRAiuTHYm69](https://goo.gl/maps/suVx2PvRAiuTHYm69).**

drapër dhe kod organizohet nga Fondacioni Kërkimor i Universitetit të Nikosisë pranë Bashkisë së Lakatamisë në kuadër të projektit Phygital  (pjesë e programit të financuar nga BE Interreg V 2014-2020 BalkanMed) me bashkëorganizimin e Programit të Arteve të Bukura, Departamenti i Dizajnit dhe Multimedias, pranë Universitetit të Nikosisë. 
Implementation partner, Thkio Ppalies: [https://thkioppalies.org/](https://thkioppalies.org/).

[Facebook: https://www.facebook.com/events/693036681164394/](https://www.facebook.com/events/693036681164394/)


[![Exhibition Flyer]({{ "/images/2019_09_20-flyer-exhibition_1st_parallel.jpg" | prepend: site.baseurl }})]({{ "/images/2019_09_20-flyer-exhibition_1st_parallel.jpg" | prepend: site.baseurl }})


[press release - en]({{ "/assets/files/2019_09_20_flubber_makeathon_info_EN.pdf" | prepend: site.baseurl }})

[press release - el]({{ "/assets/files/2019_09_20_flubber_makeathon_info_EL.pdf" | prepend: site.baseurl }})
