---
layout: post
langUrl: /al
title:  "Kultivimi i Agrikulturës së Burimeve të Hapura në Xumerkë: 15-17 Shtator 2019"
date:   2019-09-06 00:00:00 -0300
location: Kalentzi, Northern Tzoumerka, Greece
categories: newsal
featured: '/images/2019_07_16-Open_Call_MNT_01.jpg'
excerpt: "Bashkia e Xumerkës së Veriut ju fton në një mbledhje tre-ditor të titulluar 'Kultivimi i Agrikulturës së Burimeve të Hapura në Xumerkë'."
---


**Bashkia e Xumerkës së Veriut**, në bashkëpunim me **P2P Lab**, janë të lumtur të shpallin se rezultatet e vlerësimit të propozimeve për zgjidhje agrikulturore të shkallës së ulët, të dorëzuara për mbledhjen nëdrkombëtare tre-ditore, për dizajnin agrokulturor dhe atë të ndërtimit të titulluar **Tzoumakers: Kultivimi i Agrikulturës së Burimeve të Hapura në Xumerkë**.


**Bashkia e Xumerkës së Veriut** ju fton në mbledhjen tre-ditore të titulluar "Kultivimi i Agrikulturës së Burimeve të Hapura në Xumerkë". Përgjatë tre ditëve, fermerët, krijuesit, studiuesit dhe entuziastët do të eksplorojnë mënyra të reja për të përdorur teknologji të avancuara dhe tradicionale për agrikulturën lokale, nëpërmjet diskutimeve dhe krijimeve të përbashkëta të katër mjeteve dhe zgjidhjeve të design-it.


Eventi do të zhvillohet në **15-17 Shtator 2019** në makerspace-in rural të Kalenxit në Xumerkë. Pjesëmarrja në të gjitha diskutimet dhe aktivitetet është e hapur për të gjithë.


**Vendndodhja**

[https://goo.gl/maps/7dAjCLtZBSu](https://goo.gl/maps/7dAjCLtZBSu)


**Programi (pdf)**


[3DAY RETREAT PROGRAM - EN]({{ "/assets/files/2019_09_06_3DAY_RETREAT_EN_WEB.pdf" | prepend: site.baseurl }})

[3DAY RETREAT PROGRAM - EL]({{ "/assets/files/2019_09_06_3DAY_RETREAT_GR_WEB.pdf" | prepend: site.baseurl }})
