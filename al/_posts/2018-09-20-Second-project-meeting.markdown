---
layout: post
langUrl: /al
title:  "Takimi i Dytë i Projektit "
date:   2018-09-20 12:00:00 -0300
location: Athens, Greece
categories: newsal
featured: '/images/meeting2.jpg'
---

Ekipi i projektit Phygital ka qenë në **Janinë dhe Xumerkën e Veriut** për një takim dy-ditor ditën e enjte, data 13 dhe ditën e premte, data 14 Shtator 2018. Takimi u organizua nga [P2P Lab](http://www.p2plab.gr/) në bashkëpunim me [Bashkinë e Xumerkës së Veriut](http://www.voreiatzoumerka.gr/). Një takim i dytë ky për projektin në tërësi, ndërkohë i pari në të cilin mori pjesë partneri i ri i projektit nga Qirpoja, Bashkia e Lakatamisë. 

Partnerët e projektit diskutuan çështjet më të rëndësishme të projektit, me qëllim mbërritjen në një koordinim më të mirë për veprimet individuale në të treja rajonet, ato të Greqisë, Qipros dhe Shqipërisë. Në të njëjtën kohë, pjesëmarrësit patën mundësinë të vizitojnë Makerspace-in në Kalenxi, të Xumerkës së Veriut, i cili ndodhet aktualisht nën konstruksion, në mënyrë që të familjarizoheshin me zonën dhe me sfidat të cilat projekti Phygital kërkon të adresojë.

[Press release]({{ "/assets/files/PressRelease_2ndProjectMeeting.pdf" | prepend: site.baseurl }})

[Agenda]({{ "/assets/files/2ndProjectMeetingAgenda.pdf" | prepend: site.baseurl }})
