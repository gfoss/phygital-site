---
layout: post
langUrl: /al
title:  "Takimi hapës i PHYGITAL "
date:   2017-12-02 12:00:00 -0300
location: Athens, Greece
categories: newsal
featured: '/images/meeting.jpg'
---

Partneri Lider i Projektit PHYGITAL do të organizojë takimin hapës në qytetin e Athinës, në Greqi në datat 7 & 8 Dhjetor 2017. Çështjet kryesore të cilat do të diskutohen në këtë takim janë : 

Prezantimi i partnerëve të projektit PHYGITAL
Familjarizim interpersonal me ekipin e projektit PHYGITAL
Përvetësimi i konceptit, objektivave dhe rezultateve të pritshme të projektit
Analizë e thelluar e aktiviteteve të projektit në tërësi, si dhe të gjitha detyrat e alokuara për partnerët
Dakortësim për përmbushjen e detyrave kritike të projektit PHYGITAL në piketa të ndryshme kohore
Ngritja e Komitetit Drejtues të Projektit PHYGITAL
Ngritja e një rrjeti të qëndrueshëm dhe efektiv bashkëpunimi, si dhe një kuadër komunikimi


[Agenda](https://www.interreg-balkanmed.eu/gallery/Files/Project_Events/PHYGITAL/1stProjectMeetingAgenda.pdf)
