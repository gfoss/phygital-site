---
layout: post
langUrl: /al
title:  "Teknologjitë e Lira, Artet dhe Prona e Përbashkët"
date:   2019-05-14 00:00:00 -0300
location: Nicosia, Cyprus
categories: newsal
featured: '/images/2019_05_14-unconference_header.jpg'
excerpt: "Një takim interaktiv për Artin, Dizajnin, Teknologjinë, Krijimtarinë, Qytetet dhe Komunitetet e tyre"
---

**Një takim interaktic për Artin, Dizajnin, Teknologjinë, Krijimtarinë, Qytetet, dhe Komunitetet e tyre** - Fondacioni Kërkimor i Universitetit të Nikosisë, si pjesë e projektit Phygital

[https://unrf-unconference.futureworldscenter.org/](https://unrf-unconference.futureworldscenter.org/)

# Në lidhje me eventin

*Formati i takimit interaktiv*

Ky takim interaktiv ndjek momentumin e një lëvizjeje më të gjerë që po rikoncepton konferencat akademike drejt një modeli më të ndërlidhur për ndarjen e dijeve, mësimit dhe bashkëpunimit mes kolegëve. Kjo lejon prezantimin e studimeve në mënyrë interaktive, teksa pjesëmarrësit bashkëpunojnë dhe ngrejnë axhendat e tyre për diskutime, workshop-e dhe sesione të mësimit të përbashkët që i përgjigjen propozimeve të mëparshme si dhe prioriteteve që vijnë në mënyrë spontane. Ne do të ftojmë pjesëmarrës nga një rrjet i gjerë kërkuesish dhe aktivistësh në fusha të ndryshme dhe do iu krijojmë lidhje prej së largu me kolegë.

Pjesëmarrësit e takimit interaktiv janë të ftuar të kontribuojnë në një "Open Access Online Masterclass" me të njëjtën temë. Eventi do të lidhë makeathon-et lokale dhe më vonë do të çojë drejt ekspozitave që kanë për qëllim të komunikojnë vizualisht dhe në mënyrë interaktive me rezultatet e këtyre debateve. Prodhimi i një publikimi është një nga qëllimet kryesore të takimit interaktiv.

**E enjte në darkë, data 30 Maj: Fjalimi kryesor**

**E premte 31 Maj:** Një ditë prezantimesh dhe panelesh të organizuara më parë, përgjatë të cilave do të formulojmë në mënyrë kolektive një plan për të shtunën, në reflektim të prioriteteve të përcaktuara nga pjesëmarrësit.

**E shtunë, 1 Qershor** Diskutime, workshop-e dhe sesione bashkëpunimi të dizenjuara në lidhje me konkluzionet e ditës së kaluar.


**Temat Provizore**

- teknologjia e artit pjesëmarrës, artit të komunitetit dhe artit të angazhimit social

- arti i lëvizjes për software të hapur dhe të lirë

- mobilizimi i mediave sociale: e domosdoshme apo abuzive?

- komunitetet e arteve dhe teknologjive të burimeve të lira

- hapja përkundrejt lirisë në inisiativat sociale, në art dhe dizajn si dhe inisiativat teknologjike

- haktivizëm dhe artivizëm / hakimi i artit dhe arti i hakerimit 

- pyetje bashkëkohore në lidhje me artin e zhvilluar nga shoqëria civile dhe komuniteti, dizajni dhe teknologjia 

- hakimi si kulturë

- hapësirat e hakerimit dhe kultural e krijimit/ hapësirat e krijimit dhe politikat e tyre

- mësimi i përbashkët shoku-shokut

- "Dizenjo Globalisht, Prodho Lokalisht"

- dimensionet lokale vs dimensionet globale në praktikat komunitare

- teknologjia dhe arti i fjalimeve të protestave, drejtimi dhe trendet e tyre

- teknologjia e lirisë, liria e teknologjisë

- krijimi dhe prona e përbashkët: ripërkufizimi i artit publik/ arti pjesëmarrës

- prona e përbashkët dixhitale

- qeverisja dixhitale dhe lëvizjet sociale

- diskursi i vetë-organizimit

- diskursi i të dhënave publike

- kultura e rrjeteve

- teknologjia dhe politika/ politikat teknologjike

**Spikerat e ftuar**

- Richard Stallman

- Silvia Federici

- Gregory Sholette

- Luiz Guilherme Vergara

- Johan Söderberg

- Gabriele de Seta

- Harriet Poppy Speed & Dr Lynn Jones

- Ruth Catlow

**Komitetit Shkencor i Takimit Interaktiv (Lexoni më shumë për ekipin)**

Evanthia Tselika, Universiteti i Nikosisë. (Kryetar)

Chrystalleni Loizidou, hack66/ Bashkia e Lakatamisë. (Kryetar)

Helene Black, NeMe.org.

Yiannis Colakides, NeMe.org.

Maria Hadjimichael, Universiteti i Qipros.

Marios Isaakidis, University College London.

Eva Korae, Universiteti Teknologjik i Qipros.

Thrasos Nerantzis, Qendra Future World.

Leandros Savvides, Universiteti i Leicester-it.

Gabriele de Seta, Instituti i Ethnologjisë, Akademia Sinica në Taipei të Taiuanit.

Niki Sioki, Universiteti i Nikosisë.




**Vendndodhja**

University i Nikosisë

Makedonitissis 46, 

Nicosia 2417

Qipro

**Kontakt**

Për më shumë informacione mund të kontaktoni

Evanthia Tselika

Email: tselika.e@unic.ac.cy

Phone: +35722842694

[![Unconference Logo]({{ "/images/2019_05_14-unconference_LOGO.png" | prepend: site.baseurl }})]({{ "/images/2019_05_14-unconference_LOGO.png" | prepend: site.baseurl }})

[Unconference Programme - en]({{ "/assets/files/2019_05_14-Unconference_Programme.pdf" | prepend: site.baseurl }})
