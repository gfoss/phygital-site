---
layout: post
langUrl: /al
title:  "Workshop nga Krijuesit do të Krijojnë"
date:   2019-04-24 00:00:00 -0300
location: Nicosia, Cyprus
categories: newsal
featured: '/images/2019_04_24-eva workshop 2_header.jpg'
excerpt: "Fabrikimet Dixhitale për Skenën, Kostument, Pajisjet dhe Skenografinë. Një workshop i Fondacionit Kërkimor i Universitetit të Nikosisë për projektin Phygital nga Eva Korae, Eleana Alexandrou dhe Arianna Marcoulides"
---

**Një**  **Workshop nga**  **Krijuesit do të krijojnë** (Eva Korae, Eleana Alexandrou and Arianna Marcoulides) Fondacioni Kërkimor i Fondacionit të Nikosisë për projektin Phygital


Fabrikimi Dixhital i Skenës, Kostumeve, Pajisjet dhe Skenogradisë

# Fondacioni Kërkimor i Universitetit të Nikosisë


Ky workshop eksploron sesi teknologjitë e reja të fabrikimit mund të përdoren për krijimin e skenografisë për performanca kërcimi bashkëkohor. Pjesëmarrësve do u kërkohet të eksplorojnë fotografi/forma dhe t'i përkthejnë ato në mënyrë dixhitale në kostume, pajisje dhe skenografi për një performancë e cila do të zhvillohet në vjeshtë të vitit 2019 në qytetin e Limasolit dhe Nikosisë. Pjesëmarrësit do të përdorin pajisjet tona për të eksperimentuar dhe për të sjellë mendimet e tyre në jetë dhe do të kenë shansin që puna e tyre të shfaqet në performancë. Do të kreditohen publikisht artistët, idetë e të cilëve u përzgjodhën për performancën.

**Pjesëmarrja është e limituar deri në 10 vetë.**

**Pjesëmarrësit duhet të jenë në gjendje të përdorin software dizajnesh dhe do u duhet të përdorin laptop-at e tyre për workshop-in.**

Për t'u regjistruar ju lutem dërgoni e-mail tek: Tselika.e@unic.ac.cy and makerswillmake@gmail.com 

**Përfshini emrin tuaj; Email-in; Numri i telefonit dhe një biografi e shkurtër nëse është disponibël.**

Të parët që do të regjistrohen do të marrin dhe vendet e limituara që janë në dispozicion.


## Në lidhje me Krijuesit do të Krijojnë

Një iniciativë e "bytheway productions", kjo është një hapësirë për dizenjuesit në Limasol, një vend për eskperimentim, shprehje dhe krijimtari e pajisur me një prerës lazer të kapacitetit të madh që është disponibël për përdorim nga i gjithë publiku.
Inisiative ka për qëllim të jetë vendtakimi për njerëz kreativë në fushën e dezajnit, teksa ambjenti frymëzues dhe lëvizja e krijuesve modernë ofrojnë mundësi të panumërta përdorimi eksperimental të materialeve, proceseve dhe aplikacioneve. Promocioni i dezajnit, liria e shprehje, rrjetet e bashkëpunimit dhe krijimi i rrjeteve profesionale, përdoren në një ambjent pune që le hapësirë për të marrë frymëzim, për të diskutuar, për të eskperimentuar dhe për të ndarë. Nëpërmjet implementimit të eksperiencave bashkëkohore të dizenjimit dhe materializimit të nje ideje, vizitori/kreatori merr pjesë në të gjithë procesin, nga dërrasa e vizatimit dhe deri tek kontrolli i prodhimit dhe estetika e produktit final. Krijuesit nga të gjitha fushat e dezajnit (grafikët, ambjentet e brendshme, moda, arkitektura, arti, janë vetem disa prej tyre...) mund të rieskplorojnë kufijtë e artit të tyre.
Për më tepër informacione vizitoni website-in: [makerswillmake.com/](https://makerswillmake.com/)


## Phygital

**Phygital** është një program Interreg V 2014-2020 i BalkanMed, i financuar nga BE, i cili po implementohet në Greqi, Shqipëri dhe Qipro dhe përfshin zhvillimin e Makerspace-ave, nga një për çdo vend pjesëmarrës, që do të jenë në përdorim nga komunitetet lokale. Në Qipro, puna e projektit po bëhet nga Fondacioni Kërkimor i Universitetit të Nikosisë, në bashkëpunim me Bashkinë e Lakatamisë dhe hack66, duke u fokusuar në praktikat e artit social dhe tek eksplorimi i teknologjisë së hapur, artit dhe dizajnit. Projekti operon me moton dhe modelin "dizenjo globalisht, prodho lokalisht" dhe ofron metoda organizimi inovative dhe modele biznesi që do të mundësojnë një shtim të papreçedentë të njerëzve nga komuniteti që angazhohen në krijimtari të bërë vetë. Programi synon të mbështesë dhe të rrisë kapacitet lokale për inovacion dhe të shfrytëzimin e oportuniteteve që ofrojnë format e deçentralizuara të prodhimit. Seksioni qipriot i projektit ekzaminon rëndësinë e kulturës së Makerspacave për zhvillimin e artit social bashkëkohor dhe praktikat e dezajnit. Ky seksion thellohet në parimet e projekteve me burime të hapur, lirinë e software-it dhe hardware-it si dhe strukturat e bashkëpunimit nga poshtë-lart për të ekspoluar mënyrat sesi ato mund të përdoren për të adresuar nevojat e komuniteteve lokale. 

**PHYGITAL- Katalizojmë inovacionin dhe sipërmarrjen duke çliruar potencialin e produkteve dhe modeleve të biznesit në zhvillim**
- Partnerin Kryesor-GFOSS/ Shoqëria Greke e Burimeve Software të Hapura 
- Alternativa Shoku-Shokut - Greqi (e njohur ndryshe si "P2P Lab")
- Bashkia e Lakatamisë/hack66- Qipro
- Open Labs - Shqipëri
- Bashkia e Xumerkës së Veriut- Greqi
- Fondacioni Kërkimor i Universitetit të Nikosisë- Qipro
- Qendra Kombëtare për Aktivitetet Folklorike- Shqipëri



**Workshop** E shtunë, 11 maj 2019

10:00 - 15:00

**Vendndodhja**

Studioja Krijuesit do të Krijojnë

Afroditis 11

Limassol 3042

Qipro


**Kontakt**

Për më shumë informacione mund të kontaktoni

Phone: +357 99 190890

[![Workshop brochure]({{ "/images/2019_04_24-eva_workshop_2_flyer_by_makers_will_make.jpg" | prepend: site.baseurl }})]({{ "/images/2019_04_24-eva_workshop_2_flyer_by_makers_will_make.jpg" | prepend: site.baseurl }})


[press release - en]({{ "/assets/files/2019_04_24-Workshop_by_Makers_Will_Make_en.pdf" | prepend: site.baseurl }})

[press release - el]({{ "/assets/files/2019_04_24-Workshop_by_Makers_Will_Make_el.pdf" | prepend: site.baseurl }})
