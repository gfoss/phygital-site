---
layout: post
langUrl: /al
title:  "Drapër & Kod"
date:   2019-10-15 00:00:00 -0300
location: Lakatamia, Cyprus
categories: newsal
featured: '/images/2019_10_15-stickie_and_code_header.jpg'
excerpt: "Ekspozitë e Projektit Phygital. E bashkëorganizuar nga Fondacioni Kërkimor i Universitetit të Nikosisë dhe Bashkia e Lakatamisë. Një program i konceptuar për Qirpon nga Dr. Chrystalleni Loizidou me qëllim studimin e lëzivjeve bashkëkohore për një ekonomi të ndarjes së dijeve që sfidon konceptet kapitaliste të kreditimit, punës dhe prodhimit."
---


# Drapër & Çekan
**Fondacioni Kërkimor i Universitetit të Nikosisë** dhe **Bashkia e Lakatamisë**

# Vendndodhja

Muzeu i Historisë dhe Trashëgimnisë Kulturore të Lakatamisë
7 Agias Paraskevis, Lakatamia, 2311
[https://goo.gl/maps/suVx2PvRAiuTHYm69](https://goo.gl/maps/suVx2PvRAiuTHYm69)

# Hapja
24 Tetor 2019, 18:00-22:00
18:00 nga Kryetarja e Bashkisë së Lakatamisë, Dr Photoula Hadjipapa
Performancë përgjatë hapjes nga Elena Savvidou

# Fjalime Artistës nga Angelos Plessas dhe Bahar Noorizadeh
23 Tetor 2019, 19:00
Amfiteatri Unesco, Universiteti i Nikosisë

# Orët e hapjes
E mërkurë 16:00 - 20:00, E premte 16:00 - 20:00, E shtunë 15:00 - 19:00

# Kohëzgjatja e Ekspozitës
26 Tetor 2019 - 15 Shkurt 2020

# Programi Paralel i Eventeve
28 Shtator 2019 - Mars 2020

# Artistët Pjesëmarrës
Maria Andreou, Raissa Angeli, Adonis Archontides, Helen Black & Yiannis Colakides, Zach Blas, Jenny Dunn, Nihaal Faizal, Veronika Georgiou, Olga Micioska, Bahar Noorizadeh, Angelo Plessas, Tabita Rezaire, Elena Savvidou, Emiddio Vasquez

# Liderët e ekipeve të prototipave Phygital 
Ehsan Abdi, Odysseas Economides, Thrasos Nerantzis, Maria Toumazou, Kostas Tsangarides

# Dizajn |Nico Stephou

#Ekipi kurator
Peter Eramian, Elena Parpa, Evanthia (Evi) Tselika

Drapër & Kod është një ekspozitë ndërkombëtare, pjesë e Phygital, një program i konceptualizuar për Qirpon nga Dr.Chrystalleni Loizidou me qëllim studimin e lëzivjeve bashkëkohore për një ekonomi të ndarjes së dijeve që sfidon konceptet kapitaliste të kreditimit, punës dhe prodhimit." Phygital-i aktualisht po implementohet në Xumerkë të Greqisë, në Tiranë të Shqipërisë dhe Nikosi të Qirpros dhe përfshin krijimin e makerspace-ave që do të kenë fokus tek software-t e lirë, të cilët do të jenë në përdorim nga komunitetet lokale. 

Si titulli, Drapër & Kod sugjeron një kombinim veglash: drapri, një vegël agrikulturore që dikur shërbente si simbol i revolucioneve sociale; dhe kodi, gjuha e të cilit u zhvillza si një software programimi. Në debatin bashkëkohor, kodimi lidh lëvizje për software të lira, me kërkesë kryesore, në një nivel praktik, për bashkëpunim të hapur dhe rishpërndarje të teknologjisë, ndërkohë që në nivel teorik, kjo lëvizje kërkon lirinë e aksesit në krijim dhe në proceset e krijimit. Megjithëse, këto dy vegla i referohen dy sferave të pavarura të aktivitetit (fizik dhe dixhital), që të dy veglat janë krijuar për t'i dhënë formë botës në të cilën jetojmë. Për pasojë, Drapër & Kod na shpie në zemër të qëllimit të projektit Phygital, i cili ngrihet mbi praktikat që lëvizin mes materiales dhe dixhitale, duke shkrirë format e vjetra të prodhimit dhe bashkëprodhimit me format aktuale të fabrikimi dhe nocione bashkëkohore të së përbashkëtës, kolektives dhe bashkëpunueses.

Muzeu i Historisë dhe Trashëgimnisë Kulturore të Lakatamisë, në të cilin mbahet eskpozita, ndodhet në një rezidencë të vjetër të ndërtuar në mënyrë tradicionale përgjatë periudhës koloniale britanike (1922), kur materialet lokale dhe metodat e ndërtimit vernakular, aplikoheshin gjërësisht në Qipro. Muzeumio është shtëpia e një pasurie të madhe objektesh që varion nga mobiljet shtëpiake, tek veglat e femrës, duke përfshirë dhe një drapër dhe mjetet të tjera tekstili që nuk përdoren më. Këto vegla janë indikatore të historisë të Lakatamisë si një shoqëri agrikulturore, ku jeta varej nga lidhjet komunitare, dhe dijet që e kanë origjinën e tyre tek puna në natyre si dhe teknologjitë e zhvilluara me qëllim mbijetesën. Njohja e specifikës të një konteksti të tillë dhe sfida kuruese për krijimin e lidhjeve mes kulturës bashkëkohore të krijimit dhe metodave të së shkuarës për mbijetesë, zhvillon qëllimin e ekspozitës për klasifikimin dhe renditjen e objekteve. Duke përdorur një sistem të kudogjendur raftesh, ajo që edalloghet në muze është një perspektivë e re që i jepet idetë së zhvillimit dhe ekspozitës nga vetë artistët.

Më specifikisht, në Drapër dhe Kod, artistë nga Qiproja dhe e gjithë bota, do të prezantojnë punë të cilat reflektojnë, konfrontojnë dhe ri-vlerësojnë modelet aktuale të prodhimit, duke shfrytëzuar pronën e përbashkët teknologjike dhe dijet dixhitale. Për më tej, muzeumi na jep një grimpsë të jetesës së përditshme të së kaluarës së adërt dhe shërben si një terren konceptual dhe testimi për pesë prototiper Qipriote të Phygital-it, teksa makerspace-i i Lakatamisë është në konstruktim e sipër në qendrën komunitare të Bashkisë. Ky event zhvillohet në një kohë që numri i hapësirave të hakerimit dhe makerspace-ave të drejtura nga komunitetet dhe sistemet software janë të lira për përdorim nga kushdo, po shtohen gjithnjë dhe më tepër. 

Makerspace-i i Lakatamisë po zhvillohet nga Bashkia e Lakatamisë në bashkëpunim me Fondacionin Kërkimor të Universitetit të Nikosisë dhe eksploron kombinimin e teknologjive të hapura dhe të lira për t'u përdorur me artin social dhe kulturën e krijimit. Konteksti i makerspace-it, i parë së bashku me muzeumin dhe ekspozitën, na lejojnë të rikonsiderojnë mënyrën sesi ne kuptojmë dhe vlerësojmë trashëgimnitë e teknologjive, ndërtimit, jetës dhe prodhimit të përbashkët në realitete dixhitale dhe fizike (Phygital). Ekspozita do të shoqërohet nga evente paralele të cilat përfshijnë makeathone, workshop-e, udhëtime për shkollat dhe shkollat e artit, prezantime, performance dhe shfaqje. Për përditësime dhe informacione në lidhje me këto evente, ju lutem vizitoni:

[https://thkioppalies.org/Projects](https://thkioppalies.org/Projects)

Drapër & Kod organizohet nga Fondacioni Kërkimor i Universitetit të Nikosisë, pjesë e projektit Phygital (një program Interreg V 2014-2020 BalkanMed, i financuar nga BE), e bashkëorganizuar në bashkëpunim me Programin e Arteve të Bukura, Departamenti i Dizajnit dhe Multimedias dhe Bashkia e Lakatamisë. Partneri implementues është Thkio Ppalies. 


# Ekspozita dhe informacionet e programit publik
[https://thkioppalies.org/Projects](https://thkioppalies.org/Projects)
Informacioni i Kontaktit: info@thkioppalies.org

# Ture edukative për shkollat dhe shkollat e artit
Athina Kyriacou (tel. 22364097 / 22364100)
athina.kyriacou@lakatamia.org.cy / tselika.e@unic.ac.cy


# Informacione për Phygital-in
[https://phygitalproject.eu/](https://phygitalproject.eu/)

# UNRF info
[https://www.unrf.ac.cy/](https://www.unrf.ac.cy/)

# Informacione Kontakti
tselika.e@unic.ac.cy / +357 22842694


[facebook Poster]({{ "/assets/files/2019_10_15-fb_poster_draft-02.jpg" | prepend: site.baseurl }})


[press release - en]({{ "/assets/files/2019_10_15-Sickle_and_Code_press_release_en.pdf" | prepend: site.baseurl }})

[press release - el]({{ "/assets/files/2019_10_15-Sickle_and_Code_press_release_el.pdf" | prepend: site.baseurl }})
