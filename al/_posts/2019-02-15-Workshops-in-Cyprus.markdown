---
layout: post
langUrl: /al
title:  "Workshop-e nga Fondacioni Kërkimor i Universitetit të Nikosisë"
date:   2019-02-15 00:00:00 -0300
location: Egkomi, Cyprus
categories: newsal
featured: '/images/2019_02_20_workshop_header_Jenny_Dunn.jpg'
excerpt:"Përqasje e drejtuar nga dezajni për Praktikat Sociale: Krijimi i Hapësirave të Ndërhyrjes për Vendet e Testimit dhe Shkëmbimit. Një workshop i Fondacionit Kërkimor të Universitit të Nikosisë për projektin Phygital, drejtuar nga Jenny Dunn"
---

**Një**  **Workshop nga**  **Jenny Dunn** Fondacioni Kërkimor i Universitetit të Nikosisë


Përqasje e drejtuar nga dezajni për Praktikat Sociale: Krijimi i Hapësirave të Ndërhyrjes për Vendet e Testimit dhe Shkëmbimit

# Fondacioni Kërkimor i Universitetit të Nikosisë 

Ky workshop do të adresojë çështje të artit social dhe stategjitë për praktikat e dizajnit të cilat ndërtojnë lidhje komunitare, kapacitete dhe aftësi ripërtëritëse nëpërmjet prodhimit kulturor:

- Bashkëpunimi në grup, ndarja e ushqimit, kultura dhe historia
- Vënia në dukje, ndarja e ideve dhe dhënia e një platforme për zërat dhe kulturat ekzistuese
- Okupimi i hapësirave, eksperienca e prodhimit nga vetë pjesëmarrësit.

Së bashku në grupe, ne do të përdorim një përqasje të frymëzuar nga dezajni për të ndërtuar hapësira ndërhyrjeje në vendet e testimit dhe shkëmbimit; hapësira për t'u takuar, për të diskutuar dhe për të ndarë ide. Si munden të gjithë këto hapësira të lejojnë për ndarjen e eksperiencave, aftësive dhe kulturave lokale, në mënyrë që të mund të marrim pjesë në projektet dhe prodhimin e përbashkët kulturor? Ne do të diskutojmë dhe do të punojmë me proceset e dezaijnit si plani strategjik, hartëzimi, vizatimi, bërja e kolazheve, bërja e modeleve dhe prototipave.



**Workshop** E mërkurë, 20 Shkurt 2019

18.00-21.00

**Vendndodhja**

Ndërtesa e Arteve të Bukura

Michail Georgalla 29

Egkomi

Qipro

[https://goo.gl/maps/7dAjCLtZBSu](https://goo.gl/maps/7dAjCLtZBSu)

**Eventi në Facebook** 

[https://www.facebook.com/events/2195547847220820/](https://www.facebook.com/events/2195547847220820/)

**Kontakti**

Për më shumë informacione mund të kontaktoni me

Evanthia Tselika

Email: tselika.e@unic.ac.cy

Phone: +35722842694

[![Workshop brochure]({{ "/images/2019_02_20_workshop_flyer_Jenny_Dunn.jpg" | prepend: site.baseurl }})]({{ "/images/2019_02_20_workshop_flyer_Jenny_Dunn.jpg" | prepend: site.baseurl }})
