---
layout: post
langUrl: /al
title:  "Takimi Interaktiv Open Call: Teknologjitë e lira dhe me burime të hapura, Arti dhe Praktikat e Zakonshme"
date:   2018-12-07 12:00:00 -0300
location: Nicosia, Cyprus
category: newsal
---

*Teknologjitë e lira dhe me burime të hapura, Arti dhe Praktikat e Zakonshme*
*Një takim interaktiv për Artin, Dizajnin, Teknologjinë, Krijimtarinl, Qytetet dhe Komunitetet e tyre*

*31/05-02/06*

*Fondacioni Kërkimor i Universitetit të Nikosisë*

Projekti PHYGITAL. I bashkëorganizuar në bashkëpunimim në Bashkinë e Lakatamisë, hack66 dhe Programin e Arteve të Bukura, Departamenti i Dezanjit dhe Multimedias, pranë Universitetit të Nikosisë.

*Çfarë lloj kreativiteti do të lindë pas kulturave dixhitale të së sotmes? Pas krijimit të qyteteve smart, pas survejimit, pas inovacionit, kur sipërmarrja sociale të ketë mbyllur ciklin e vet?*

 Përgjatë viteve të fundit ka pasur një trend në rritje të shtimit të grupeve dhe hapësirave të bashkëpunimit, të cilat operohen nga grupe dhe struktura komunitare bashkëpunimi dhe proçese mësimi të përbashkët (si në terren ashtu dhe online). Kjo po ndodh paralelisht me debatet më të mëdha në lidhje me fatin e hapësirave publike, hapjes, lirisë së aksesit dhe mënyrës sesi teknologjitë e reja po influencojnë trajtën sesi formëzohet arti social i orientuar drejt komunitetit, dezanji dhe praktikat teknologjike.

Takimi interaktiv sjell së bashku kërkues dhe praktikues të fushave të ndryshme, për t'u takuar, për të ndarë njohuritë dhe për të bashkëpunuar rreth çështjeve që kanë të bëjnë me të gjithë komunitetin dixhital, lëvizjet për art dhe teknologji të lirë, si dhe struktura kolektive dhe komunitare të bashkëpunimit dhe proceseve të përbashkëta pedagogjike (në terren si dhe online).

*Ne ftojmë të interesuar të marrin pjesë me prezantime 15-20 minutëshe, panele, abstrakte, postera dhe propozime workshop-esh dhe takimesh deri në datën 28/02/2019.*

*Përditësimet mund të gjenden më poshtë në*

[here](http://www.unrf.ac.cy/free-and-open-source-technologies-arts-and-commoning-practices-an-unconference-about-art-design-technology-making-cities-and-their-communities/)

*Ose në*

[here](https://fineartuniccy.wordpress.com/conferences-and-academic-meetings/free-and-open-source-technologies-arts-and-commoning-practices-an-unconference-about-art-design-technology-making-cities-and-their-communities/)


*Pjesëmarrja në takimin interaktiv do të jetë falas.*

*Për të propozuar diçka ju lutem dërgoni :*

Panele: Abstrakt prej 250 fjalësh të të gjithë prezantuesve, biografikë të shkurtëer, detajet e kontaktit dhe titulli i panelit

Studime: Abstrakt prej 250 fjalësh, biografi e shkurtër, detajet e kontakteve dhe titulli i studimit

Workshop: Abstrakt prej 250 fjalësh, biografi e shkurtër, detajet e kontaktit dhe titulli i kërkimit

Ide të tjera: Abstrakt prej 250 fjalësh/ përmbledhje e konceptit, biografi e shkurtër dhe detajet e kontaktit

Dorëzo informacionet e mësipërme tek: [cycommons@protonmail.com](mailto:cycommons@protonmail.com)

Për çdo pyetje mund të dërgoni e-mail tek [tselika.e@unic.ac.cy](mailto:tselika.e@unic.ac.cy) ose tek [chrystalleni.loizidou@gmail.com](mailto:chrystalleni.loizidou@gmail.com)


[Open Call PDF]({{ "/assets/files/UNCONFERENCE_tech_art_commons_UNRF_2019.pdf" | prepend: site.baseurl }})
