---
layout: post
langUrl: /al
title:  "Workshop i zhvilluar nga Fondacioni Kërkimor i Universitetit të Nikosisë për projektin Phygital"
date:   2019-02-26 00:00:00 -0300
location: Egkomi, Cyprus
categories: newsal
featured: '/images/2019_03_08_workshop_flyer_by_natalia_rozalia_avlona.jpg'
excerpt: "Teknologjitë e hapura: Historia e tyre, Ligjet dhe Gjinitë. Një workshop i organizuar nga Fondacioni Kërkimor i Universitetit të Nikosisë për projektin Phygital nga Natalia- Rozalia Avlona"
---

**Një**  **Workshop nga**  **Natalia- Rozalia Avlona** Fondacioni Kërkimor i Universitetit të Nikosisë për projektin Phygital


Teknologjitë e hapura: Historitë e tyre, Ligjet dhe Gjinitë

# Fondacioni Kërkimor i Universitetit të Nikosisë 

## Programi

|  |  |
|:----------------|:-------------|
| 14:30-14:45 | Prezantimi |
| 14:45-16:15 | Nga grupet e punës Dixhitale drejt Ekonomisë së Platformave: Një prezantim i historisë së tyre,<br> çështjet ligjore, politikat, dhe kultura|
| 16:15-16:45 | Diskutim |
| 16:45-17:15 | Pushim |
| 17:15-18:15 | Gjinia dhe Teknologjitë e Hapura: Perspektivat feministe, sfidat dhe potenciali |
| 18:15-19:30 | Workshop i titulluar "Queering your wiki" |
| 19:30-20:00 | Diskutim i Hapur |

_Ju lutem sillni laptopin tuaj me vete._

Përgjatë katër dekadave të fundit, grupet e punës dixhitale i kanë dhënë formë terrenit të lirë, pjesëmarrës dhe gjithëpërfshirës të prodhimit të të mirave jo-materiale. Krijimi i World Wide Web, i Prodhimit të Përbashkët, FOSS-it dhe lëvizjet për Kulturën e Lirë, kanë formëzar trajta të reja drejtimi dhe prodhimi si dhe vegla ligjore për komunitetet virtuale, ku mund të thuhet se u krijua "ethosi i hakerimit". Vlera e ndarjes, të qënit i hapur, bashkëpunimit dhe decentralizimit morën rëndësi të veçantë, teksa vlerat post-kapitaliste kanë sfiduar nocionin e "pronësisë" si një subjektin dhe thelbin e debatit tradicional për çështjen e të drejtave të autorit. Në anën tjetër, përgjatë dekadës së fundit, rritja e shpejtë e një sektori të ri ekonomik të platformave elektronike ka ndryshuar konceptet dhe strukturën e "ndarjes" dhe "decentralizimit", për shkak të bashkëpunimit dixhital, në atë që mund të emërtojmë kapitalizëm "netarkik". 


Vitet e fundit, shtimi në numër i makerspace-ave, të cilat shërbejnë si hapësira të lira, të drejtuara nga vetë komunitetet, ku software-t dhe hardware-t e lira përdoren në mënyrë bashkëpunuese nga individët (Kostakis, Niaros & Drechsler 2017) ka ndryshuar praktikat dixhitale të sjelljes, përderisa përdoruesit shtojnë dhe përmirësojnë gjatë përdorimit teknologjitë e prodhimit (printerat 3D, makineritë CNC) duke hyrë kështu në terrenin e "phygital-it", pra atë të prodhimit të të mirave materiale të prekshme. Megjithatë, teksa këto praktika pionere po demokratizojne teknologjinë, duke sfiduar strukturat dominante ligjore dhe teknike, nuk është ende e qartë sesa gjithëpërfshirëse do të jenë. Për pasojë perspektivat feministe në lidhje me teknologjitë e hapura mbeten ende për t'u ekspolurar si një metodë dhe mjet për të ekspozuar sfidat dhe potencialet e këtyre teknologjive.
Ky workshop do të ofrojë një rishikim sistematik të kësaj fushe, duke u fokusuar përgjatë pjesës së parë në aspektet historike dhe legale të teknologjive të hapura dhe përgjatë pjesës së dytë, në perspektivat feministe në lidhje me sfidat që krijohen kur risitë e teknologjisë konfrontohen me çështjet gjinore.


## Rreth Natalia-Rozalia Avlona

**Natalia-Rozalia Avlona** është një avokate dhe studiuese, e cila aktualisht është duke punuar si asistente kërkimore për projektin TARGET të ELIAMEP (Fondacioni Helenik për Politikat Europian dhe të Huaja) në Athinë.

Ajo ka studiuar Drejtësi në Fakultetin e Drejtësisë të Universitetit Kombëtar Kapodhistria të Athinës (2006), si dhe ka kryer studimet Master në Drejtësi pranë Universitetit "King's College London" (2007). Më pas ajo ka ndjekur leksione në departamentin e Gjeografisë Kulturore në Royal Holloway, të Universitetit të Londrës si dhe në departamentin e Kurimit të Artit Kontemporan në Kolegjin Ruajal të Artit, po në Londër. Aktualisht, ajo është doktorante në Universitetin Kombëtar Teknik të Athinës, ku po shkruan tezën me temë "Teknologjitë e Hapura: Historia e tyre, Ligjet dhe Gjinitë".

Ekspertiza e saj është në fushën e bashkëpunimit dixhital dhe mënyra sesi Teknologjia, Ligjet dhe teoritë gjinore janë të ndërlidhura në këtë fushë.

Avlona ka një eksperiencë ndërkombëtare pune në një serë organizatash dhe programesh bashkëpunimi Europian, në Mbretërinë e Bashkuar, në Belgjikë dhe në Greqi. Mes këtyre organizatave është Universiteti "Aristoteli" i Selanikut, Organizata për Pronësinë Industriale në Greqi, Kolegji Mbretëror i Arteve në Londër, "Braktisni Mjetet e Zakonshme" në Mançester, Departamenti i së Ardhmes të Teknologjive në Zhvillim (DG Connect, Komisioni i BE), Sekretariati i Përgjithshëm për Barazinë Gjinore, si dhe Rrjeti i Universiteteve Greke, Athinë. 

Ajo është anëtare e Komitetit të Menaxhimit për Kostot e Aksionit CA16121- Nga ndarja tek përkujdesja: Ekzaminimi i Aspekteve Socio-Teknike të Ekonomisë Bashkëpunuese (2017-2021).

Momentalisht ajo është pjesë e ekipit kërkimor të ELIAME-s, i cili po punon në projektin TARGET (Ndërmarrja e një Përqasjeje Reflektive për Barazinë Gjinore dhe Transformimin Gjinor), një program kërkimi që ka për qëllim të kontribuojë avancimin e barazisë gjinore në fushën e Kërkimit dhe Inovacionit.  

Përpos karrierës së saj akademike, Avlona ka një përfshirje të madhe si aktiviste në fushën e bashkëpunimit teknologjik dhe barazisë gjinore, teksa ka drejtuar një seri leksionesh në Wikipedia për Galeri, Librari, Arkiva dhe Muzeume, si dhe ka bashkëorganizuar workshope feministe tek FOSS për hackerspace.gr, si dhe takime interaktive në lidhje me bashkëpunimin teknologjik. Përgjatë tre viteve të fundit, ajo është anëtare e Zonës Ekonomike të Solidaritetit Social.


## Phygital

**Phygital-i** është një program i Interreg-ut V 2014-2020 BalkanMed, i financuar nga BE, i cili po implementohet në Greqi, Shqipëri dhe Qipro dhe përfshin zhvillimin e makerspace-ave, nga një në çdo vend pjesëmarrës në program, të cilat do të përdoren nga komunitetet lokale. Në Qipro, puna e projektit po ndërmerret nga Qendra Kërkimore e Universitetit të Nikosisë në bashkëpunim me Bashkinë e Lakatamisë dhe hack66 dhe do të fokusohet në praktikat e artit social, të cilat përfshijnë teknologjinë e hapur, artin dhe dizajnin. Projekti operon me moton dhe modelin "dizenjo globalisht - prodho lokalisht", i cili prezanton sjelljhe inovative organizacionale dhe modele biznesi, që do të mundësojnë një rritje galopante të numrit të njerëzve nga komunitetet lokale që do të angazhohen në aktivitete pune të pavarur dhe individuale. Projekti synon mbështetjen dhe përforcimin e kapaciteteve lokale për inovacion dhe të shfrytëzojë mundësitë që deçentralizimi i mjeteve të prodhimit mund të krijojë. Sektori i Qipros për projektin shqyrton rëndësinë e kulturës së Makerspace-ave për zhvillimin e artit kontemporan social dhe për praktikat e dizajnit. Ky seksion thellohet në parimet e projekteve me burime të hapura, liria e software-t dhe hardware-t dhe strukturave të bashkëpunimit nga poshtë-lart për të eksploruar mënyra në të cilat ato do të mund të përdoren, në linjë me praktikat sociale të dizajnit, për të adresuar nevojat e komunitetit. 

**PHYGITAL- Katalizojmë inovacionin dhe sipërmarrjen duke çliruar potencialin e produksionit në rritje dhe modeleve të biznesit**
- Partneri Kryesor GFOSS/ Shoqëria Greke e Burimeve të Hapura Software 
- Alternative Shoku-Shokut - Greqi (e njohur ndryshe si "P2P Lab")
- Bashkia e Lakatamisë/ hack66- Qipro
- Open Labs - Shqipëri
- Bashkia e Xumerkës së Veriut- Greqi
- Fondacioni Kërkimor i Universitetit të Nikosisë (UNRF)- Qipro
- Qendra Kombëtare e Aktiviteteve Folklorike- Shqipëri


**Workshop** E premte, 08 Mars 2019

14:30 - 20:00

**Vendndodhja**

Godina e Arteve të Bukura

Michail Georgalla 29

Egkomi

Qipro

[https://goo.gl/maps/7dAjCLtZBSu](https://goo.gl/maps/7dAjCLtZBSu)


**Contact**

For more information you can contact

Evanthia Tselika

Email: tselika.e@unic.ac.cy

Phone: +35722842694

[![Workshop brochure]({{ "/images/2019_03_08_workshop_flyer_by_natalia_rozalia_avlona.jpg" | prepend: site.baseurl }})]({{ "/images/2019_03_08_workshop_flyer_by_natalia_rozalia_avlona.jpg" | prepend: site.baseurl }})


[press release - en]({{ "/assets/files/2019_02_26_press_release_en.pdf" | prepend: site.baseurl }})

[press release - el]({{ "/assets/files/2019_02_26_press_release_gr.pdf" | prepend: site.baseurl }})
