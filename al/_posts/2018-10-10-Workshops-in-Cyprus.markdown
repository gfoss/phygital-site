---
layout: post
langUrl: /al
title:  "Workshop i zhvilluar nga Fondacioni Kërkimor në Universitin e Nikosisë"
date:   2018-10-10 00:00:00 -0300
location: Nicosia, Cyprus
categories: newsal
featured: '/images/flyer_leandros_workshop_featured.jpg'
excerpt: "Krijimi dhe ndërlidhja në një botë të pakonektuar:  Hapësirat e hakerimit si përfaqësime të politikave tekno-sociale në realizmin bashkëkohor të kapitalizmit. Një Workshop i drejtuar nga Leandros Savvides i Fondacionit Kërkimor në Universitetin e Nikosisë"
---

**Një**  **Workshop nga**  **Leandros Savvides** Fondacioni Kërkimor i Universitetit të Nikosisë 


Krijimi dhe ndërlidhja në një botë të pakonektuar: Hapësirat e hakerimit si përfaqësime të politikave tekno-sociale në realizmin bashkëkohor të kapitalizmit.

# Fondacioni Kërkimor i Universitetit të Nikosisë
 
Workshop-i do të adresojë zhvillimin e kulturës së krijimit, në hapësira ku teknologjia dhe krijimtaria ndodhen jashtë laboratorëve profesionalë, kryesisht në hapësirat e Hakerimit, në Makerspaces dhe në Fab Labs. Hapësira të tilla përbëjnë site të rëndësishme në zhvillimin e hardware-ve dhe software-ve të hapura dhe ofrojnë kushte të favorshme për përhapjen e teknologjisë të krijuar lokalisht për publikun e informuar në fushën e teknologjisë, por edhe më tej. Në mënyrë specifike, ky workshop do të adresojë konvergjencën mes aktivizmit dhe kulturës së krijimit brenda kapitalizmit realist. Duke reflektuar mbi rezultatet e kërkimit tim, argumentoj se politikat e hapësirave të Hakerimit/Makerspaces dhe Fab Labs përbëjnë site që ezaurojnë kontradiktat dhe ofrojnë hapësira për të kundërshtuar paradigmat dominante dhe për të reflektuar mbi shoqëritë në të cilat atop operojnë. Përdoruesit shndërrohen në zhvillues dhe njëkohësisht rizbulojnë forma të reja konsumi, me anë të proçeseve mësimore dhe një rekonceptualizim të marrëdhënieve mes shkencës dhe artizanati, produktivitetit dhe argëtimit, autoritetit dhe mësimit horizontal prej të tjerëve. Ndonëse këto praktika kanë një natyrë të dukshme sociale dhe kolektive, në zemër të kulturës së krijimit ekziston dhe një frymë indidualizmi. 


**Prezantimi dhe Dialogu-** E enjte, 11 Tetor 2018

18.00-21.00

**Seance e Workshop-it dhe e hedhjes së ideve** - E shtunë, 13 Tetor 2018

11.00-14.00

**Lakatamia/ Event i Komunitetit hack66** - E shtunë, 13 Tetor 2018

14.00 dhe më tej

Vendndodhja

Makerspace

Qendra Polydynamo Lakatamia

Aigaiou, Lakatamia

[https://goo.gl/maps/DPVRyBPUmj72](https://goo.gl/maps/DPVRyBPUmj72)



Për më shumë informacione mund të kontaktoni

Evanthia Tselika- tselika.e@unic.ac.cy

[![Workshop brochure]({{ "/images/flyer_leandros_workshop.jpg" | prepend: site.baseurl }})]({{ "/images/flyer_leandros_workshop.jpg" | prepend: site.baseurl }})
