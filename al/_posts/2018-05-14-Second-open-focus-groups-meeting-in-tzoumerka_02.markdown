---
layout: post
langUrl: /al
title:  "Takimi i dytë i hapur i Focus Group-it në Xumerka"
date:   2018-05-14 12:00:00 -0300
location: Athens, Greece
categories: newsal
featured: '/images/Poster_FocusGroup2_featured.png'
---

Takimi i dytë i hapur i Focus Group-it do të zhvillohet në qendrën kulturore pranë Bashkisë së Kalenxit në datën 14 Maj. 

Çështjet kryesore që do të diskutohen janë


* Sfidat, bashkëpunimi dhe implementimi i burimeve të hapura për aplikacionet agrokulturore në Xumerkën Veriore
* Diskutim dhe prioritarizim i pajisjeve të nevojshme në makerspace-in që është duke u krijuar 

[Invitation (in greek)]({{ "/assets/files/Invitation_FG2.pdf" | prepend: site.baseurl }})

[Press release (in greek)]({{ "/assets/files/D3.2.2.prePressRelease_FocusGroup2.pdf" | prepend: site.baseurl }})

[Agenda (in greek)]({{ "/assets/files/Agenda_FG2.pdf" | prepend: site.baseurl }})
