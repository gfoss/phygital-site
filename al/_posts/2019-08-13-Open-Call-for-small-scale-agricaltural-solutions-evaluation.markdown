---
layout: post
langUrl: /al
title:  "Thirrje e Hapur për Zgjidhje Agrikulturore të Shkallës së Ulët– Vlerësimi i Propozimeve"
date:   2019-08-13 00:00:00 -0300
location: Kalentzi, Northern Tzoumerka, Greece
categories: newsal
featured: '/images/2019_07_16-Open_Call_MNT_01.jpg'
excerpt: "Bashkia e Xumerkës së Veriut, në bashkëpunim me laboratorin P2P, janë të lumtur të shpallin rezultatet e vlerësimit të propozimeve për zgjidhje agrikulturore të shkallës së ulët të dorëzuara për mbledhjen ndërkombëtare tri-ditore të dizenjimit agrikulturor dhe ndërtimit, të titulluar “Tzoumakers: Kultivimi i Agrikulturës së Burimeve të Hapura në Xumerkë”."
---


**Bashkia e Xumerkës së Veriut**, në bashkëpunim me **P2P Lab**, janë të lumtur të shpallin rezultatet e vlerësimit të propozimeve për zgjidhje agrikulturore të shkallës së ulët të dorëzuara për mbledhjen ndërkombëtare tri-ditore të dizenjimit agrikulturor dhe ndërtimit, të titulluar **“Tzoumakers: Kultivimi i Agrikulturës së Burimeve të hapura në Xumerkë”**.

Gjithsej, tetë (8) aplikacione u dorëzuan përpara afatit përfundimtar nga ekipet lokale dhe ndërkombëtare të krijuesve, fermerëve dhe entuziastëve. Të gjitha aplikacionet ofruan zgjidhje interesante të shkallës së vogël dhe dizajnit të hapur në sektorin agrokulturor. Shfrytëzojmë rastin për të falenderuar përzemërsisht të gjithë ata që u përfshinë në zhvillimin dhe dorëzimin e ideve, ndonëse vetëm 4 prej zgjidhjeve të propozuara janë përzgjedhur për të marrë pjesë në këtë event.

Përzgjedhja është kryer në bazë të një serie kriteresh të thjeshtëzuara, në lidhje me zhvillimin e zgjidhjes, procesin dhe karakteristikat e implementimit të tij, së bashku me impaktin e pritshëm për sektorin agrokulturor.

Rezultatet e evaluimit e të gjithë propozimeve të dorëzuara janë disponibël [këtu](https://gitlab.com/phygitalproject/phygitalproject.gitlab.io/blob/master/assets/files/Selection.pdf).

Vlen të nënvizohet fakti se evaluimi i propozimeve në këtë stad nuk duhet të konsiderohet si përfundimtar nga palët e përfshira. Ky evaluim shpreh vlerësimet e zgjidhjeve të propozuara, bazuar në informacionin e dhënë në format e aplikimit. Pjesëmarrja eventuale dhe zhvillimi i zgjidhjeve të propozuara janë subjekt i disponueshmërisë së aplikantëve, fizibilitetit dhe mundësisë reale për të materializuar zgjidhjen e propozuar në kontekstin e eventit, si dhe një sërë faktorësh të paparashikuar. Në rast mos-përmbushjeje të kushteve të ekipeve të propozuara, organizatorët do të procedojnë me përzgjedhjen e zgjidhjes me rezultatin më të lartë.





